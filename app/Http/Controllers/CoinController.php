<?php

namespace App\Http\Controllers;


use App\Coin;
use App\CoinSocial;
use App\Jobs\CoinSync;
use App\Mail\CoinListing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CoinController extends Controller
{

    public function index()
    {
        $coins = Coin::all();

        $data = [];

        if (count($coins)) {

            CoinSync::dispatch($coins);

            foreach ($coins as $coin) {
                $mn_count[$coin->name] = $coin->mn_count;
                $volume[$coin->name] = $coin->volume;
                $market_cap[$coin->name] = $coin->volume_usd * $coin->circulation;
                $price[$coin->name] = $coin->volume_usd;
                $change[$coin->name] = $coin->change;
                $daily_reward[$coin->name] = $coin->mn_count > 0 ? (float)$coin->paid_rewards_mn / (float)$coin->mn_count : 0;
                $roi_days[$coin->name] = $mn_count[$coin->name] > 0 ? ceil((float)$coin->mn_required_coins / (float)$daily_reward[$coin->name]) : 0;
                $roi_percent[$coin->name] = $daily_reward[$coin->name] > 0 ? 365 / ($roi_days[$coin->name])* 100 : 0;
                $mn_worth[$coin->name] = $coin->mn_required_coins * $coin->volume_usd;
            }

            $data = [
                'mn_count' => $mn_count,
                'coins' => $coins,
                'usd_24h_vol' => $volume,
                'market_cap' => $market_cap,
                'usd' => $price,
                'usd_24h_change' => $change,
                'daily_reward' => $daily_reward,
                'roi_days' => $roi_days,
                'roi_percent' => $roi_percent,
                'mn_worth' => $mn_worth,
            ];
        }
        return view('home')->with($data);
    }

    public function create()
    {
        return view('create_coin');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:4',
            'ticker' => 'required|min:2',
            'icon_url' => 'required|min:6',
            'display_type' => 'required',
        ];

        $this->validate($request, $rules);


        switch ($request->display_type) {
            case 1 :
                $request->merge([
                    'ico' => '1',
                    'coming_soon' => '0',
                    'maintenance' => '0',
                ]);
                break;
            case 2 :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '1',
                    'maintenance' => '0',
                ]);
                break;
            case 3 :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '0',
                    'maintenance' => '1',
                ]);
                break;
            default :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '0',
                    'maintenance' => '0',
                ]);
        }

        $data = request()->except("_token", "display_type");

        $coin = Coin::firstOrCreate($data);

        if ($coin) {
            return redirect()->route('home')->with("message", $request->name . " Inserted Successfully");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function show($name)
    {
        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $dollar = $btc = 0;

        foreach ($json as $obj) {
            if ($obj->code == 'USD') $btc = $obj->rate;
        }
        $coin = Coin::whereName($name)->first();
        return view('show_coin', compact('coin', 'btc'));
    }

    public function edit($id)
    {
        $coin = Coin::find($id);
        return view('edit_coin', compact('coin'));

    }

    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|min:4',
            'ticker' => 'required|min:2',
            'paid_rewards_mn' => 'required|min:4',
            'mn_required_coins' => 'required|min:3',
            'icon_url' => 'required|min:3',
        ];

        $this->validate($request, $rules);


        $ticker_lower = strtolower($request->ticker);

        $exchange_api_url = "https://api.coingecko.com/api/v3/simple/price?ids={$ticker_lower}&vs_currencies=usd&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true";
        $circulation_supply_api_url = "https://chainz.cryptoid.info/{$ticker_lower}/api.dws?q=circulating";
        $ticker_lower = $ticker_lower == 'pivx' ? 'piv' : $ticker_lower;
        $mn_count_api_url = "https://www.coinexplorer.net/api/v1/{$ticker_lower}/masternode/count";

        switch ($request->display_type) {
            case 1 :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '0',
                    'maintenance' => '0',
                    'simple' => '1',
                ]);
                break;
            case 2 :
                $request->merge([
                    'ico' => '1',
                    'coming_soon' => '0',
                    'maintenance' => '0',
                    'simple' => '0',
                ]);
                break;
            case 3 :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '1',
                    'maintenance' => '0',
                    'simple' => '0',
                ]);
                break;
            case 4 :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '0',
                    'maintenance' => '1',
                    'simple' => '0',
                ]);
                break;
            default :
                $request->merge([
                    'ico' => '0',
                    'coming_soon' => '0',
                    'maintenance' => '0',
                    'simple' => '0',
                ]);
        }

        $request->merge([
            'exchange_api_url' => $exchange_api_url,
            'circulation_supply_api_url' => $circulation_supply_api_url,
            'mn_count_api_url' => $mn_count_api_url,
        ]);

        $coin = Coin::find($id);
        $coin->name = $request->name;
        $coin->ticker = $request->ticker;
        $coin->exchange_api_url = $request->exchange_api_url;
        $coin->circulation_supply_api_url = $request->circulation_supply_api_url;
        $coin->mn_count_api_url = $request->mn_count_api_url;
        $coin->paid_rewards_mn = $request->paid_rewards_mn;
        $coin->mn_required_coins = $request->mn_required_coins;
        $coin->mn_block_reward = $request->mn_block_reward;
        $coin->notes = $request->notes;
        $coin->icon_url = $request->icon_url;
        $coin->ico = $request->ico;
        $coin->coming_soon = $request->coming_soon;
        $coin->maintenance = $request->maintenance;
        $coin->simple = $request->simple;
        $coin->save();

        return redirect()->route('coins.index')->with("message", $coin->name . " Update Successfully");
    }

    public function destroy($id)
    {
        $coin = Coin::destroy($id);

        if ($coin) {
            return redirect()->back()->with('message', 'Coin Deleted Successfully');
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function social($id)
    {
        $coin = Coin::find($id);
        return view('create_social', compact('coin'));
    }

    public function social_store(Request $request)
    {
        $data = request()->except("_token", "coin_name");

        $social = CoinSocial::firstOrCreate($data);

        if ($social) {
            return redirect()->route('home')->with("message", $request->coin_name . " Social Medias Of The Coin Successfully Inserted");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function social_update(Request $request, $id)
    {
        $update = CoinSocial::updateOrCreate([
            'coin_id' => $request->coin_id,
        ], [
            'twitter' => $request->twitter,
            'btc_talk' => $request->btc_talk,
            'explorer' => $request->explorer,
            'github' => $request->github,
            'website' => $request->website,
            'exchange1' => $request->exchange1,
            'exchange_name1' => $request->exchange_name1,
            'exchange2' => $request->exchange2,
            'exchange_name2' => $request->exchange_name2,
            'exchange3' => $request->exchange3,
            'exchange_name3' => $request->exchange_name3,
        ]);
        if ($update) {
            return redirect()->route('home')->with("message", $request->coin_name . " Social Medias Of The Coin Successfully Updated");
        }

        return redirect()->back()->with("error", "Something went wrong");
    }

    public function coinMail(Request $request)
    {
        $rules = [
            "name" => "required|min:3|max:50",
            "ticker" => "required|min:3|max:10",
            "explorer_url" => "required|url",
            "github_url" => "required|url",
            "website_url" => "required|url",
            "bitcointalk_url" => "required|url",
            "discord_url" => "required|url",
            "g-recaptcha-response" => "required|string",
        ];
        $messages = [
            "name.required" => "Coin Name is required",
            "name.min" => "Coin Name is must be at least 3 characters",
            "name.max" => "Coin Name is must be maximum 10 characters",
            "ticker.required" => "Coin Ticker is required",
            "ticker.min" => "Coin Ticker is must be at least 3 characters",
            "ticker.max" => "Coin Ticker is must be maximum 10 characters",
            "explorer_url.required" => "Explorer Url is required",
            "explorer_url.url" => "Explorer Url is must be a valid URL",
            "github_url.required" => "Explorer Url is required",
            "github_url.url" => "Github Url is must be valid URL",
            "website_url.required" => "Explorer Url is required",
            "website_url.url" => "Website Url is must be valid URL",
            "bitcointalk_url.required" => "Explorer Url is required",
            "bitcointalk_url.url" => "BitcoinTalk Url is must be valid URL",
            "discord_url.required" => "Explorer Url is required",
            "discord_url.url" => "Discord Url is must be valid URL",
        ];

        $this->validate($request,$rules,$messages);

        $request = $request->all();

        $control_humanity = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfbbpMUAAAAAN88KL17_3zYLKscbNuoO4VHPV2g&response=" . $request['g-recaptcha-response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $control_humanity = json_decode($control_humanity);

        if($control_humanity->success == false){
            return redirect()->back()->withErrors(['Whoopss.. Are we human?']);
        }

        $mail  = Mail::to('groupghetto@gmail.com')
            ->send(new CoinListing($request));

        Session::flash('status','Your Coin Listing Request Sended Successfully');
        return redirect()->route('welcome');

    }
}
