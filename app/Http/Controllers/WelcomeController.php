<?php

namespace App\Http\Controllers;

use App\Coin;
use App\Jobs\CoinSync;
use GuzzleHttp\Client;
use madmis\ExchangeApi\Client\GuzzleClient;

class WelcomeController extends Controller
{
    public function index()
    {

        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $btc = 0;

        foreach ($json as $obj) {
            if ($obj->code == 'USD') $btc = $obj->rate;
        }

        $coins = Coin::all();
        $data = [];

        if (count($coins) > 0) {

            CoinSync::dispatch($coins);

            foreach ($coins as $coin) {
                $mn_count[$coin->name] = $coin->mn_count;
                $volume[$coin->name] = $coin->volume;
                $market_cap[$coin->name] = $coin->volume_usd * $coin->circulation;
                $price[$coin->name] = $coin->volume_usd;
                $change[$coin->name] = $coin->change;
                $daily_reward[$coin->name] = $coin->mn_count > 0 ? (float)$coin->paid_rewards_mn / (float)$coin->mn_count : 0;
                $roi_days[$coin->name] = $mn_count[$coin->name] > 0 ? ceil((float)$coin->mn_required_coins / (float)$daily_reward[$coin->name]) : 0;
                $roi_percent[$coin->name] = $daily_reward[$coin->name] > 0 ? 365 / ($roi_days[$coin->name])* 100 : 0;
                $mn_worth[$coin->name] = $coin->mn_required_coins * $coin->volume_usd;
            }

            $data = [
                'btc' => $btc,
                'mn_count' => $mn_count,
                'coins' => $coins,
                'usd_24h_vol' => $volume,
                'market_cap' => $market_cap,
                'usd' => $price,
                'usd_24h_change' => $change,
                'daily_reward' => $daily_reward,
                'roi_days' => $roi_days,
                'roi_percent' => $roi_percent,
                'mn_worth' => $mn_worth,
            ];

        } else {
            $data = [
                'btc' => $btc,
            ];
        }

        return view('welcome')->with($data);
    }
}
