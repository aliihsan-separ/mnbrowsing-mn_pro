<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $dollar = $btc = 0;

        foreach ($json as $obj) {
            if ($obj->code == 'USD') $btc = $obj->rate;
        }
        View::share('btc', $btc);
    }

}
