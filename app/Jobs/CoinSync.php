<?php

namespace App\Jobs;

use App\Coin;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CoinSync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $coins;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($coins)
    {
        $this->coins = $coins;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->coins as $coin) {

            $curl_handle1=curl_init();
            curl_setopt($curl_handle1, CURLOPT_URL,$coin->exchange_api_url);
            curl_setopt($curl_handle1, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle1, CURLOPT_USERAGENT, 'MNBROWSING1');
            $query_exchange = curl_exec($curl_handle1);
            curl_close($curl_handle1);
            $exchange = json_decode($query_exchange,true)[strtolower($coin->name)];

            $curl_handle2=curl_init();
            curl_setopt($curl_handle2, CURLOPT_URL,$coin->mn_count_api_url);
            curl_setopt($curl_handle2, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle2, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle2, CURLOPT_USERAGENT, 'MNBROWSING2');
            $query_mn_count = curl_exec($curl_handle2);
            curl_close($curl_handle2);


            if ($query_mn_count === false) {
                $mn_count = json_decode(file_get_contents($coin->mn_count_api_url),true);
            }else{
                $mn_count = json_decode($query_mn_count,true)['result']['active'];
            }

            $curl_handle3=curl_init();
            curl_setopt($curl_handle3, CURLOPT_URL,$coin->circulation_supply_api_url);
            curl_setopt($curl_handle3, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle3, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle3, CURLOPT_USERAGENT, 'MNBROWSING3');
            $query_circulation = curl_exec($curl_handle3);
            curl_close($curl_handle3);
            $circulation =json_decode($query_circulation,true);

            $updated_coin = Coin::find($coin->id);
            $updated_coin->volume = $exchange['usd_24h_vol'];
            $updated_coin->volume_usd = $exchange['usd'];
            $updated_coin->mn_count = $mn_count > 0 ? $mn_count : $updated_coin->mn_count;
            $updated_coin->circulation = $circulation;
            $updated_coin->market_cap = $exchange['usd'] * $circulation;
            $updated_coin->price = $exchange['usd'];
            $updated_coin->change = $exchange['usd_24h_change'];
            $updated_coin->daily_reward = (float)$coin->paid_rewards_mn / (float)($mn_count > 0 ? $mn_count : $updated_coin->mn_count);
            $updated_coin->roi_days = (float)$coin->paid_rewards_mn / (10000 / ((float)$coin->mn_required_coins / (float)($mn_count > 0 ? $mn_count : $updated_coin->mn_count)));
            $updated_coin->roi_percent = 365 / ((float)$coin->paid_rewards_mn / (((float)$coin->paid_rewards_mn / (float)($mn_count > 0 ? $mn_count : $updated_coin->mn_count)))* 100 );
            $updated_coin->mn_worth = (float)$coin->mn_required_coins * $exchange['usd'];
            $updated_coin->save();
        }
    }
}
