<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoinSocial extends Model
{
    use SoftDeletes;
    protected $table = 'coin_socials';
    protected $guarded = ['created_at','updated_at','deleted_at'];

}
