<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coin extends Model
{
    use SoftDeletes;

    protected $table = 'coins';
    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    public function social()
    {
        return $this->hasOne(CoinSocial::class);
    }

}
