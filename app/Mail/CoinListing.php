<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CoinListing extends Mailable
{
    use Queueable, SerializesModels;
    protected $coinArr;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($coinArr)
    {
        $this->coin_infos = $coinArr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $coin_info = $this->coin_infos;

        return $this->from('no-reply@mnbrowsing.com')
            ->subject('You Have A Coin Listing Request')
            ->view('coin-listing-mail',compact('coin_info'));
    }
}
