<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Sync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync All Coins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        /**
        return coin id,symbol and name
         */
        $response1 = $client->get('https://api.coingecko.com/api/v3/coins/list');

        /**
         * return coin btc price
         */
        $response2 = $client->get('https://api.coingecko.com/api/v3/simple/price?ids=dash&vs_currencies=btc');



        dd(json_decode((string)$response1->getBody()));
    }
}
