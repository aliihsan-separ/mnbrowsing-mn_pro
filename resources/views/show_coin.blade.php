@extends('layouts.app')

@push('styles')
    <style>
        a{ color: #7e7ee8 !important }
    </style>
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row rounded mb-20" style="border: 1px solid #6c757d!important; background: rgba(0,0,0,0.5);">
            <div class="col-md-5 col-xs-12 d-flex align-items-center" style="height: 90px;">
                <div style="border-right: 1px solid #383838; width: 100%;">
                    <img src="{!! $coin->icon_url !!}" alt="" width="50">
                    <span class="font-size-h2 ml-3">
                        {!! strtoupper($coin->name) !!}
                    </span>

                </div>
            </div>
            <div class="col-12 d-block d-sm-block d-md-none d-lg-none d-xl-none">
                <hr class="hr-3">
            </div>
            <div class="col-md-7 col-xs-12 d-flex align-items-center justify-content-center" style="height: 90px;">
                <div class="float-left d-none d-sm-block">
                    <button class="btn btn-outline-purple">Statics</button>
                    <button class="btn btn-outline-purple">Specifications</button>
                    <button class="btn btn-outline-purple">Exchanges</button>
                    <button class="btn btn-outline-purple">Guide</button>
                    <button class="btn btn-outline-purple">List</button>
                    <button class="btn btn-outline-purple">Map</button>
                </div>
            </div>

            <hr class="style-one d-none d-sm-block"/>

            <div class="col-12 m-5" style="height: 80px !important;">
                <div class="row d-flex justify-content-center">
                    <div class="col-1">
                        <a href="{!! isset($coin->social->website) ? $coin->social->website : 'javascript:void(0)' !!}" target="_blank">
                            <i class="fas fa-globe fa-2x"></i><br>
                            <small style="color:#7e7ee8 !important;">Website</small>
                        </a>

                    </div>
                    <div class="col-1 d-none d-sm-block">
                        <a href="{!! isset($coin->social->github) ? $coin->social->github : 'javascript:void(0)' !!}" target="_blank">
                            <i class="fab fa-github fa-2x"></i><br>
                            <small style="color:#7e7ee8 !important;">Github</small>
                        </a>
                    </div>
                    <div class="col-1">
                        <a href="{!! isset($coin->social->marketcap) ? $coin->social->marketcap : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fas fa-chart-area fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Marketcap Site</small>
                        </a>
                    </div>
                    <div class="col-1 d-none d-sm-block">
                        <a href="{!! isset($coin->social->explorer) ? $coin->social->explorer : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fas fa-search fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Block Explorer</small>
                        </a>
                    </div>
                    <div class="col-1">
                        <a href="{!! isset($coin->social->twitter) ? $coin->social->twitter : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fab fa-twitter fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Twitter</small>
                        </a>
                    </div>
                    <div class="col-1 d-none d-sm-block">
                        <a href="{!! isset($coin->social->facebook) ? $coin->social->facebook : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fab fa-facebook fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Facebook</small>
                        </a>
                    </div>
                    <div class="col-1">
                        <a href="{!! isset($coin->social->discord) ? $coin->social->discord : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fab fa-discord fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Discord</small>
                        </a>
                    </div>
                    <div class="col-1 d-none d-sm-block">
                        <a href="{!! isset($coin->social->reddit) ? $coin->social->reddit : 'javascript:void(0)' !!}" target="_blank">
                        <i class="fab fa-reddit fa-2x"></i><br>
                        <small style="color:#7e7ee8 !important;">Reddit</small>
                        </a>
                    </div>
                </div>

            </div>
        </div>


        <div class="clearfix my-50"></div>


        <div class="row mt-20">
            <div class="col-12 col-xl-8">
                <div class="row  justify-content-center">
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box1">
                            <div class="col-12" style="padding: 10px 0;">
                                @php( $roi_days = ceil((float)$coin->mn_required_coins / (float)$coin->daily_reward))
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!!  number_format((float)(365 / ($roi_days)* 100), 2, '.', ',') !!}%</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-chart-line"></i> <span style="color:#7b7b7b !important;">  Annual ROI  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box1">
                            <div class="col-12" style="padding: 10px 0; ">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)$coin->mn_worth ,'2','.',',') !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-money-bill"></i> <span style="color:#7b7b7b !important;">  MasterNodes Cost  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box1">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!! $coin->mn_required_coins .' '. strtoupper($coin->ticker)!!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-plug"></i> <span style="color:#7b7b7b !important;">  Required Coins  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box1">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!! $coin->mn_count !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-share-alt"></i> <span style="color:#7b7b7b !important;">  Enabled Masternodes  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row my-20  justify-content-center">
                    <div class="col-12 col-md-6 col-lg-6 ">
                        <div class="row border rounded box2">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!! number_format((float)$coin->circulation, 0, '.', ',') .' '. strtoupper($coin->ticker) !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-parachute-box"></i> <span style="color:#7b7b7b !important;"> Coin Supply </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-6 ">
                        <div class="row border rounded box2">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)$coin->market_cap,'2','.',',' ) !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-chart-area"></i> <span style="color:#7b7b7b !important;"> Market Cap </span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row my-20 justify-content-center">
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box3">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{{ $coin->mn_block_reward .' '.strtoupper($coin->ticker) }}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-map-pin"></i> <span style="color:#7b7b7b !important;"> MasterNode Block Reward  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box3">
                            <div class="col-12" style="padding: 10px 0; ">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! $coin->volume_usd !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-map-pin"></i> <span style="color:#7b7b7b !important;"> Coin Price </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box3">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!! number_format((float)$coin->daily_reward,8, '.', ',') . ' '. $coin->ticker !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-cubes"></i> <span style="color:#7b7b7b !important;"> Daily Earned </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box3">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">{!!  number_format((float)($coin->mn_required_coins * $coin->mn_count), 0, '.', ',') .' '. strtoupper($coin->ticker) !!}</span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-lock"></i> <span style="color:#7b7b7b !important;"> Coins Locked </span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row mt-20 justify-content-center">
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box4">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)($coin->daily_reward * $coin->volume_usd), 2, '.', ',') !!}
                                    <br>
                                    <small>{!! number_format((float)($coin->daily_reward * $coin->volume_usd),8, '.', ',') . ' '. $coin->ticker !!}</small>
                                </span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-stopwatch"></i> <span style="color:#7b7b7b !important;">  Daily Income  </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box4">
                            <div class="col-12" style="padding: 10px 0; ">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)($coin->daily_reward * $coin->volume_usd * 7), 2, '.', ',') !!}<br>
                                    <small>{!! number_format((float)($coin->daily_reward * $coin->volume_usd * 7),8, '.', ',') . ' '. $coin->ticker !!}</small>
                                </span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-clock"></i> <span style="color:#7b7b7b !important;"> Weekly Income </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box4">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)($coin->daily_reward * $coin->volume_usd * 30), 2, '.', ',') !!}<br>
                                    <small>{!! number_format((float)($coin->daily_reward * $coin->volume_usd * 30),8, '.', ',') . ' '. $coin->ticker !!}</small>
                                </span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-calendar"></i> <span style="color:#7b7b7b !important;"> Monthly Income </span>
                                </span>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3 ">
                        <div class="row border rounded box4">
                            <div class="col-12" style="padding: 10px 0;">
                                <span style="width: 100%; color: #fff; padding: 10px 0;">${!! number_format((float)($coin->daily_reward * $coin->volume_usd * 365), 2, '.', ',') !!} <br>
                                    <small>{!! number_format((float)($coin->daily_reward * $coin->volume_usd * 365),8, '.', ',') . ' '. $coin->ticker !!}</small>
                                </span>
                                <hr class="style-one"/>
                                <span>
                                    <i class="fas fa-calendar-alt"></i> <span style="color:#7b7b7b !important;"> Yearly Income </span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-12 col-xl-4" style="margin: 0 !important; padding: 0 !important;">
                <div id="doughnutChart" class="chart"></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function(){
            $("#doughnutChart").drawDoughnutChart([
                { title: "Coins Locked",value : {{ (float)$coin->mn_count*(float)$coin->mn_required_coins }} ,  color: "#e76d3b" },
                { title: "Circulation", value:  {{ $coin->circulation }},   color: "#2dbda8" },
                /*{ title: "New York",      value:  70,   color: "#6DBCDB" },
                { title: "London",        value : 50,   color: "#F7E248" },
                { title: "Sydney",        value : 40,   color: "#D7DADB" },
                { title: "Berlin",        value : 20,   color: "#FFF" }*/
            ]);
        });

        ;(function($, undefined) {
            $.fn.drawDoughnutChart = function(data, options) {
                var $this = this,
                    W = $this.width(),
                    H = $this.height(),
                    centerX = W/2,
                    centerY = H/2,
                    cos = Math.cos,
                    sin = Math.sin,
                    PI = Math.PI,
                    settings = $.extend({
                        segmentShowStroke : true,
                        segmentStrokeColor : "#0C1013",
                        segmentStrokeWidth : 1,
                        baseColor: "rgba(0,0,0,0.5)",
                        baseOffset: 4,
                        edgeOffset : 10,//offset from edge of $this
                        percentageInnerCutout : 75,
                        animation : true,
                        animationSteps : 90,
                        animationEasing : "easeInOutExpo",
                        animateRotate : true,
                        tipOffsetX: -8,
                        tipOffsetY: -45,
                        tipClass: "doughnutTip",
                        summaryClass: "doughnutSummary",
                        summaryTitle: "TOTAL:",
                        summaryTitleClass: "doughnutSummaryTitle",
                        summaryNumberClass: "doughnutSummaryNumber",
                        beforeDraw: function() {  },
                        afterDrawed : function() {  },
                        onPathEnter : function(e,data) {  },
                        onPathLeave : function(e,data) {  }
                    }, options),
                    animationOptions = {
                        linear : function (t) {
                            return t;
                        },
                        easeInOutExpo: function (t) {
                            var v = t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t;
                            return (v>1) ? 1 : v;
                        }
                    },
                    requestAnimFrame = function() {
                        return window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame ||
                            function(callback) {
                                window.setTimeout(callback, 1000 / 60);
                            };
                    }();

                settings.beforeDraw.call($this);

                var $svg = $('<svg width="' + W + '" height="' + H + '" viewBox="0 0 ' + W + ' ' + H + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>').appendTo($this),
                    $paths = [],
                    easingFunction = animationOptions[settings.animationEasing],
                    doughnutRadius = Min([H / 2,W / 2]) - settings.edgeOffset,
                    cutoutRadius = doughnutRadius * (settings.percentageInnerCutout / 100),
                    segmentTotal = 0;

                //Draw base doughnut
                var baseDoughnutRadius = doughnutRadius + settings.baseOffset,
                    baseCutoutRadius = cutoutRadius - settings.baseOffset;
                $(document.createElementNS('http://www.w3.org/2000/svg', 'path'))
                    .attr({
                        "d": getHollowCirclePath(baseDoughnutRadius, baseCutoutRadius),
                        "fill": settings.baseColor
                    })
                    .appendTo($svg);

                //Set up pie segments wrapper
                var $pathGroup = $(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
                $pathGroup.attr({opacity: 0}).appendTo($svg);

                //Set up tooltip
                var $tip = $('<div class="' + settings.tipClass + '" />').appendTo('body').hide(),
                    tipW = $tip.width(),
                    tipH = $tip.height();

                //Set up center text area
                var summarySize = (cutoutRadius - (doughnutRadius - cutoutRadius)) * 3,
                    $summary = $('<div class="' + settings.summaryClass + '" />')
                        .appendTo($this)
                        .css({
                            width: summarySize + "px",
                            height: summarySize + "px",
                            "margin-left": -(summarySize / 2) + "px",
                            "margin-top": -(summarySize / 2) + "px"
                        });
                var $summaryTitle = $('<p class="' + settings.summaryTitleClass + '">' + settings.summaryTitle + '</p>').appendTo($summary);
                var $summaryNumber = $('<p class="' + settings.summaryNumberClass + '"></p>').appendTo($summary).css({opacity: 0});

                for (var i = 0, len = data.length; i < len; i++) {
                    segmentTotal += data[i].value;
                    $paths[i] = $(document.createElementNS('http://www.w3.org/2000/svg', 'path'))
                        .attr({
                            "stroke-width": settings.segmentStrokeWidth,
                            "stroke": settings.segmentStrokeColor,
                            "fill": data[i].color,
                            "data-order": i
                        })
                        .appendTo($pathGroup)
                        .on("mouseenter", pathMouseEnter)
                        .on("mouseleave", pathMouseLeave)
                        .on("mousemove", pathMouseMove);
                }

                //Animation start
                animationLoop(drawPieSegments);

                //Functions
                function getHollowCirclePath(doughnutRadius, cutoutRadius) {
                    //Calculate values for the path.
                    //We needn't calculate startRadius, segmentAngle and endRadius, because base doughnut doesn't animate.
                    var startRadius = -1.570,// -Math.PI/2
                        segmentAngle = 6.2831,// 1 * ((99.9999/100) * (PI*2)),
                        endRadius = 4.7131,// startRadius + segmentAngle
                        startX = centerX + cos(startRadius) * doughnutRadius,
                        startY = centerY + sin(startRadius) * doughnutRadius,
                        endX2 = centerX + cos(startRadius) * cutoutRadius,
                        endY2 = centerY + sin(startRadius) * cutoutRadius,
                        endX = centerX + cos(endRadius) * doughnutRadius,
                        endY = centerY + sin(endRadius) * doughnutRadius,
                        startX2 = centerX + cos(endRadius) * cutoutRadius,
                        startY2 = centerY + sin(endRadius) * cutoutRadius;
                    var cmd = [
                        'M', startX, startY,
                        'A', doughnutRadius, doughnutRadius, 0, 1, 1, endX, endY,//Draw outer circle
                        'Z',//Close path
                        'M', startX2, startY2,//Move pointer
                        'A', cutoutRadius, cutoutRadius, 0, 1, 0, endX2, endY2,//Draw inner circle
                        'Z'
                    ];
                    cmd = cmd.join(' ');
                    return cmd;
                };
                function pathMouseEnter(e) {
                    var order = $(this).data().order;
                    $tip.text(data[order].title + ": " + data[order].value)
                        .fadeIn(200);
                    settings.onPathEnter.apply($(this),[e,data]);
                }
                function pathMouseLeave(e) {
                    $tip.hide();
                    settings.onPathLeave.apply($(this),[e,data]);
                }
                function pathMouseMove(e) {
                    $tip.css({
                        top: e.pageY + settings.tipOffsetY,
                        left: e.pageX - $tip.width() / 2 + settings.tipOffsetX
                    });
                }
                function drawPieSegments (animationDecimal) {
                    var startRadius = -PI / 2,//-90 degree
                        rotateAnimation = 1;
                    if (settings.animation && settings.animateRotate) rotateAnimation = animationDecimal;//count up between0~1

                    drawDoughnutText(animationDecimal, segmentTotal);

                    $pathGroup.attr("opacity", animationDecimal);

                    //If data have only one value, we draw hollow circle(#1).
                    if (data.length === 1 && (4.7122 < (rotateAnimation * ((data[0].value / segmentTotal) * (PI * 2)) + startRadius))) {
                        $paths[0].attr("d", getHollowCirclePath(doughnutRadius, cutoutRadius));
                        return;
                    }
                    for (var i = 0, len = data.length; i < len; i++) {
                        var segmentAngle = rotateAnimation * ((data[i].value / segmentTotal) * (PI * 2)),
                            endRadius = startRadius + segmentAngle,
                            largeArc = ((endRadius - startRadius) % (PI * 2)) > PI ? 1 : 0,
                            startX = centerX + cos(startRadius) * doughnutRadius,
                            startY = centerY + sin(startRadius) * doughnutRadius,
                            endX2 = centerX + cos(startRadius) * cutoutRadius,
                            endY2 = centerY + sin(startRadius) * cutoutRadius,
                            endX = centerX + cos(endRadius) * doughnutRadius,
                            endY = centerY + sin(endRadius) * doughnutRadius,
                            startX2 = centerX + cos(endRadius) * cutoutRadius,
                            startY2 = centerY + sin(endRadius) * cutoutRadius;
                        var cmd = [
                            'M', startX, startY,//Move pointer
                            'A', doughnutRadius, doughnutRadius, 0, largeArc, 1, endX, endY,//Draw outer arc path
                            'L', startX2, startY2,//Draw line path(this line connects outer and innner arc paths)
                            'A', cutoutRadius, cutoutRadius, 0, largeArc, 0, endX2, endY2,//Draw inner arc path
                            'Z'//Cloth path
                        ];
                        $paths[i].attr("d", cmd.join(' '));
                        startRadius += segmentAngle;
                    }
                }
                function drawDoughnutText(animationDecimal, segmentTotal) {
                    $summaryNumber
                        .css({opacity: animationDecimal})
                        .text((segmentTotal * animationDecimal).toFixed(1));
                }
                function animateFrame(cnt, drawData) {
                    var easeAdjustedAnimationPercent =(settings.animation)? CapValue(easingFunction(cnt), null, 0) : 1;
                    drawData(easeAdjustedAnimationPercent);
                }
                function animationLoop(drawData) {
                    var animFrameAmount = (settings.animation)? 1 / CapValue(settings.animationSteps, Number.MAX_VALUE, 1) : 1,
                        cnt =(settings.animation)? 0 : 1;
                    requestAnimFrame(function() {
                        cnt += animFrameAmount;
                        animateFrame(cnt, drawData);
                        if (cnt <= 1) {
                            requestAnimFrame(arguments.callee);
                        } else {
                            settings.afterDrawed.call($this);
                        }
                    });
                }
                function Max(arr) {
                    return Math.max.apply(null, arr);
                }
                function Min(arr) {
                    return Math.min.apply(null, arr);
                }
                function isNumber(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                }
                function CapValue(valueToCap, maxValue, minValue) {
                    if (isNumber(maxValue) && valueToCap > maxValue) return maxValue;
                    if (isNumber(minValue) && valueToCap < minValue) return minValue;
                    return valueToCap;
                }
                return $this;
            };
        })(jQuery);
    </script>
@endpush
