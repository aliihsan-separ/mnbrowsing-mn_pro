<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>MN Browsing</title>

    <meta name="description" content="MN Browsing Coin Listing Platform">
    <meta name="author" content="Group Ghetto">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="MN Browsing">
    <meta property="og:site_name" content="MN Browsing">
    <meta property="og:description" content="MN Browsing">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{!! asset('images/icon.ico') !!}">
    <link rel="icon" type="image/png" sizes="192x192" href="{!! asset('images/icon.ico' ) !!}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!! asset('images/icon.ico') !!}">
    <!-- END Icons -->

    <!-- Stylesheets -->

    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{!! asset('assets/css/codebase.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <style>
        a { color: #7e7ee8 !important }
        #logo { margin-left: 4px; }
        #logo:hover { text-decoration: underline; }
    </style>
</head>
<body style="color:#7e7ee8 !important;">

<div id="page-container" class=" page-header-modern main-content-boxed">
    <div class="curtain"></div>
    <header id="page-header" style="background: rgba(0,0,0,0.7) !important;">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div class="content-header-section">
                    <h2 class="mb-4">
                        <a href="{!! url('/') !!}" style="">
                            <img src="{!! asset('images/logo-i.png') !!}" alt="{!! config('app.name') !!}" width="65"><span id="logo" style="font-size:21px; color:#7e7ee8 !important">MN BROWSING</span>
                        </a>
                    </h2>
            </div>

            <div class="content-header-section text-right">
                <a href="{{ route('coin-listing') }}" class="btn btn-outline-purple"> Coin Listing</a>
            </div>
            <!-- END Left Section -->
        </div>
        <!-- END Header Content -->
    </header>

    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="content">
            @include('layouts.alerts')
            <div class="row">
                <div class="col-md-8 float-left">
                    {{--<ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" style="color:#7e7ee8 !important" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#">List New Coin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#">Support</a>
                        </li>
                    </ul>--}}
                </div>

                <div class="col-md-3 float-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active" style="color:#7e7ee8 !important" href="https://discordapp.com/invite/pSqydjn"><i
                                    class="fab fa-discord fa-2x"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="https://github.com/MNBrowsing/MN-Browsing-Coin"><i
                                    class="fab fa-github-square fa-2x"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:#7e7ee8 !important" href="#"><i
                                    class="fab fa-twitter fa-2x"></i></a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="row">
                <section class="container-banner">
                    <div class="curtain-2"></div>
                    <div class="square square1"></div>
                    <div class="square square2"></div>
                    <div class="square square3"></div>
                    <div class="square square4"></div>
                </section>
            </div>

            <div class="my-50 text-center">

                <div class="row js-appear-enabled animated fadeIn" data-toggle="appear">
                    <div class="col-12">

                        <div class="block" style="background: rgba(0,0,0,0.5) !important; color:#7e7ee8 !important">
                            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" id="myTab" role="tablist"
                                style="background: rgba(0,0,0,0.5) !important; color:#7e7ee8 !important;">
                                <li class="nav-item"
                                    style="background: rgba(0,0,0,0.5) !important; border-bottom: 1px solid white !important;">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#statics" role="tab"
                                       aria-controls="home" aria-selected="true">STATICS</a>
                                </li>
                                <li class="nav-item"
                                    style="background: rgba(0,0,0,0.5) !important; border-bottom: 1px solid white !important;">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#simple-statics" role="tab"
                                       aria-controls="profile" aria-selected="false">SIMPLE STATICS</a>
                                </li>
                                <li class="nav-item"
                                    style="background: rgba(0,0,0,0.5) !important; border-bottom: 1px solid white !important;">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#ico" role="tab"
                                       aria-controls="contact" aria-selected="false">ICO</a>
                                </li>
                                <li class="nav-item"
                                    style="background: rgba(0,0,0,0.5) !important; border-bottom: 1px solid white !important;">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#coming-soon" role="tab"
                                       aria-controls="contact" aria-selected="false">COMING SOON</a>
                                </li>
                                <li class="nav-item"
                                    style="background: rgba(0,0,0,0.5) !important; border-bottom: 1px solid white !important;">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#maintenance" role="tab"
                                       aria-controls="contact" aria-selected="false">MAINTENANCE</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">

                                <div class="tab-pane fade show active p-20 pt-10" id="statics" role="tabpanel"
                                     aria-labelledby="statics-tab">
                                    <div id="DataTables_Table_1_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table
                                                    class="table table-bordered table-striped table-vcenter js-dataTable-simple dataTable no-footer"
                                                    id="DataTables_Table_1" role="grid">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1"
                                                            colspan="1"
                                                            aria-label="Name: activate to sort column ascending">Name
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                                                            VOLUME
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting_desc"
                                                            style="width: 15%;" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                            aria-label="Access: activate to sort column ascending"
                                                            aria-sort="descending">
                                                            MARKET CAP
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">PRICE
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">CHANGE
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">ROI
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">MN WORTH
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @if(isset($coins) && count($coins))
                                                        @foreach($coins as $coin)
                                                            @if($coin->ico == '0' && $coin->maintenance == '0' && $coin->coming_soon == '0')
                                                            <tr role="row" class="odd">
                                                                <td class="text-center" width="10%">
                                                                    <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                                    <a href="{!! route('coin.name',$coin->name) !!}"><strong style="color: #7e7ee8 !important">{!! $coin->name !!}</strong></a>
                                                                </td>
                                                                <td class="font-w600">{!! number_format((float)$usd_24h_vol[$coin->name], 2, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$market_cap[$coin->name], 2, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell" style="color: {!! $usd_24h_change[$coin->name] < 0 ? 'red' : 'green' !!}">{!! $usd[$coin->name] !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell"><span
                                                                        style="color: {!! $usd_24h_change[$coin->name] < 0 ? 'red' : 'green' !!}">{!! number_format((float)$usd_24h_change[$coin->name], 2, '.', ',') !!}% <i
                                                                            class="fa fa-angle-double-{!! $usd_24h_change[$coin->name] < 0 ? 'down' : 'up' !!}"></i> </span>
                                                                </td>
                                                                <td class="d-none d-sm-table-cell" style="color: #007bff;">{!! number_format((float)$roi_percent[$coin->name], 2, '.', ',').' % / '.(int)$roi_days[$coin->name] !!}
                                                                    Days
                                                                </td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$mn_worth[$coin->name], 3, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p-20 pt-10" id="simple-statics" role="tabpanel"
                                     aria-labelledby="simple-statics-tab">
                                    <div id="DataTables_Table_1_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table
                                                    class="table table-bordered table-striped table-vcenter js-dataTable-simple dataTable no-footer"
                                                    id="DataTables_Table_1" role="grid">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1"
                                                            colspan="1"
                                                            aria-label="Name: activate to sort column ascending">NAME
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">ANNUAL ROI
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">MN COUNT
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">REQUIRED
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">DAILY
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">MONTHLY
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">YEARLY
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @if(isset($coins) && count($coins))
                                                        @foreach($coins as $coin)
                                                            @if($coin->ico == '0' && $coin->maintenance == '0' && $coin->coming_soon == '0')
                                                                @php
                                                                    $monthly_reward_show = $daily_reward[$coin->name] * 30;
                                                                    $yearly_reward_show = $daily_reward[$coin->name] * 360;
                                                                @endphp
                                                                <tr role="row" class="odd">
                                                                <td class="text-center" width="10%">
                                                                    <a href="{!! route('coin.name',$coin->name) !!}">
                                                                        <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                                        <strong>{!! $coin->name !!}</strong>
                                                                    </a>
                                                                </td>
                                                                <td class="font-w600">{!! number_format((float)$roi_percent[$coin->name], 2, '.', ',') !!}%</td>
                                                                <td class="d-none d-sm-table-cell">{!!  $mn_count[$coin->name] !!}</td>
                                                                <td class="d-none d-sm-table-cell">{!! $coin->mn_required_coins .' '. strtoupper($coin->ticker) !!} </td>
                                                                    <td class="d-none d-sm-table-cell">{!! number_format((float)$daily_reward[$coin->name],2,'.',',') . ' ' . $coin->ticker!!}  </td>
                                                                    <td class="d-none d-sm-table-cell">{!! number_format((float)$monthly_reward_show,2,'.',',') . ' ' . $coin->ticker !!} </td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$yearly_reward_show,2,'.',',') . ' ' . $coin->ticker !!} </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p-20 pt-10" id="ico" role="tabpanel"
                                     aria-labelledby="ico-tab">
                                    <div id="DataTables_Table_1_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table
                                                    class="table table-bordered table-striped table-vcenter js-dataTable-simple dataTable no-footer"
                                                    id="DataTables_Table_1" role="grid">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1"
                                                            colspan="1"
                                                            aria-label="Name: activate to sort column ascending">Name
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                                                            VOLUME
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting_desc"
                                                            style="width: 15%;" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                            aria-label="Access: activate to sort column ascending"
                                                            aria-sort="descending">
                                                            MARKET CAP
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">PRICE
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">CHANGE
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">ROI
                                                        </th>
                                                        <th class="text-center sorting_disabled" style="width: 15%;"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Profile">MN WORTH
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @if(isset($coins) && count($coins))
                                                        @foreach($coins as $coin)
                                                            @if($coin->ico == '1')
                                                            <tr role="row" class="odd">
                                                                <td class="text-center" width="10%">
                                                                    <a href="{!! route('coin.name',$coin->name) !!}">
                                                                        <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                                        <strong>{!! $coin->name !!}</strong>
                                                                    </a>
                                                                </td>
                                                                <td class="font-w600">{!! number_format((float)$usd_24h_vol[$coin->name], 2, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$market_cap[$coin->name], 2, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell">{!! $usd[$coin->name] !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                                <td class="d-none d-sm-table-cell"><span
                                                                        style="color: {!! $usd_24h_change[$coin->name] < 0 ? 'red' : 'green' !!}">{!! number_format((float)$usd_24h_change[$coin->name], 2, '.', ',') !!}% <i
                                                                            class="fa fa-angle-double-{!! $usd_24h_change[$coin->name] < 0 ? 'down' : 'up' !!}"></i> </span>
                                                                </td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$roi_percent[$coin->name], 2, '.', ',').' % / '.(int)$roi_days[$coin->name] !!}
                                                                    Days
                                                                </td>
                                                                <td class="d-none d-sm-table-cell">{!! number_format((float)$mn_worth[$coin->name], 3, '.', ',') !!}
                                                                    <i class="fa fa-usd"></i></td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p-20 pt-10" id="coming-soon" role="tabpanel"
                                     aria-labelledby="coming-soon-tab">
                                    <div id="DataTables_Table_1_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table
                                                    class="table table-bordered table-striped table-vcenter js-dataTable-simple dataTable no-footer"
                                                    id="DataTables_Table_1" role="grid">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1"
                                                            colspan="1"
                                                            aria-label="Name: activate to sort column ascending">Name
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                                                            NOTES
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting_desc"
                                                            style="width: 15%;" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                            aria-label="Access: activate to sort column ascending"
                                                            aria-sort="descending">
                                                            UPDATE
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @if(isset($coins) && count($coins))
                                                        @foreach($coins as $coin)
                                                            @if($coin->coming_soon)
                                                                <tr role="row" class="odd">
                                                                    <td class="text-center" width="10%">
                                                                        <a href="{!! route('coin.name',$coin->name) !!}">
                                                                            <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                                            <strong>{!! $coin->name !!}</strong>
                                                                        </a>
                                                                    </td>
                                                                    <td class="font-w600" width="45%">{!! $coin->notes !!}</td>
                                                                    <td class="font-w600" width="45%">{!! \Carbon\Carbon::parse($coin->updated_at)->format('l jS \\of F Y h:i:s T') !!}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade p-20 pt-10" id="maintenance" role="tabpanel"
                                     aria-labelledby="maintenance-tab">
                                    <div id="DataTables_Table_1_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table
                                                    class="table table-bordered table-striped table-vcenter js-dataTable-simple dataTable no-footer"
                                                    id="DataTables_Table_1" role="grid">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1"
                                                            colspan="1"
                                                            aria-label="Name: activate to sort column ascending">Name
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                                                            NOTES
                                                        </th>
                                                        <th class="d-none d-sm-table-cell sorting_desc"
                                                            style="width: 15%;" tabindex="0"
                                                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                                                            aria-label="Access: activate to sort column ascending"
                                                            aria-sort="descending">
                                                            UPDATE
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>


                                                    @if(isset($coins) && count($coins))
                                                        @foreach($coins as $coin)
                                                            @if($coin->maintenance)
                                                            <tr role="row" class="odd">
                                                                <td class="text-center" width="10%">
                                                                    <a href="{!! route('coin.name',$coin->name) !!}">
                                                                        <img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20">
                                                                        <strong>{!! $coin->name !!}</strong>
                                                                    </a>
                                                                </td>
                                                                <td class="font-w600" width="45%">{!! $coin->notes !!}</td>
                                                                <td class="font-w600" width="45%">{!! \Carbon\Carbon::parse($coin->updated_at)->format('l jS \\of F Y h:i:s T') !!}</td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" style="background: rgba(0,0,0,0.9); position: absolute; bottom: 0; width: 100%;">
        <div class="content py-20 font-size-xs clearfix">
            {{--<div class="float-right">
                Developed with <i class="fa fa-heart" style="color: red;"></i> by
                <a class="font-w600" href="https://www.groupghetto.com" target="_blank">GG Dev</a>
            </div>--}}
            <div class="float-left">
                <span class="font-w600">Copyright</span> &copy; <span class="js-year-copy">2019</span> MN Browsing
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{!! asset('assets/js/core/jquery.min.js') !!}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="{!! asset('assets/js/core/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.appear.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.countTo.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/js.cookie.min.js') !!}"></script>
<script src="{!! asset('assets/js/codebase.js') !!}"></script>
<!-- Page JS Plugins -->
<script src="{!! asset('assets/js/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') !!}"></script>
<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/be_tables_datatables.js') !!}"></script>

<script src="{!! asset('assets/js/plugins/chartjs/Chart.bundle.min.js') !!}"></script>

<!-- Page JS Code -->
{{--<script src="{!! asset('assets/js/pages/be_pages_crypto_dashboard.js') !!}"></script>--}}

</body>
</html>
