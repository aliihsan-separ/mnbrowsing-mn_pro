<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>MN Browsing</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <meta name="description" content="MN Browsing Coin Listing Platform">
    <meta name="author" content="Group Ghetto">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="MN Browsing">
    <meta property="og:site_name" content="MN Browsing">
    <meta property="og:description" content="MN Browsing">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{!! asset('images/icon.ico') !!}">
    <link rel="icon" type="image/png" sizes="192x192" href="{!! asset('images/icon.ico' ) !!}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!! asset('images/icon.ico') !!}">
    <!-- END Icons -->

    <!-- Stylesheets -->

    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{!! asset('assets/css/codebase.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <style>
        a { color: #7e7ee8 !important }
        #logo { margin-left: 4px; }
        #logo:hover { text-decoration: underline; }
        .form-control {
            background: #2f3134;
            border-color: #424448;
            color: #7e7ee8;
        }
        .form-control:focus {
            outline: none !important;
            border:1px solid #7e7ee8;
            box-shadow: 0 0 10px #7e7ee8;
            background: #2f3134;
            color: #7e7ee8;
        }
        .form-control::placeholder{
            color: #7c7c7c;
        }
        .text-center {
            text-align: center;
        }

        .g-recaptcha {
            display: inline-block;
        }
    </style>
</head>
<body style="color:#7e7ee8 !important;">

<div id="page-container" class=" page-header-modern main-content-boxed">
    <div class="curtain"></div>
    <header id="page-header" style="background: rgba(0,0,0,0.7) !important;">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div class="content-header-section">
                <h2 class="mb-4">
                    <a href="{!! url('/') !!}" style="">
                        <img src="{!! asset('images/logo-i.png') !!}" alt="{!! config('app.name') !!}" width="65"><span id="logo" style="font-size:21px; color:#7e7ee8 !important">MN BROWSING</span>
                    </a>
                </h2>
            </div>
            <!-- END Left Section -->
        </div>
        <!-- END Header Content -->
    </header>

<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div class="content">
    @include('layouts.alerts')
        <div class="row">
            <div class="col-md-6">
                <div class="block rounded">
                    <div class="block-header bg-black-op-25">
                        <h3 class="block-title text-center">How To List Your Coin?</h3>
                    </div>
                    <div class="block-content">
                        <ul style="line-height: 2">
                            <li>For listing your coin in MN Browsing you have to do post in Facebook and Twitter about listing your coin in MN Browsing.</li>
                            <li>Also you have to announce in your BitcoinTalk Account. Join Discord Server and send info on coin listing section.</li>
                            <li>Now Login in MN Browsing panel and go to Coin List Section. Fill up below basic details of your coin.
                                <ul>
                                    <li>Coin Name</li>
                                    <li>Coin Ticker</li>
                                    <li>Explorer URL</li>
                                    <li>Github URL</li>
                                    <li>Website URL</li>
                                    <li>BitcoinTalk URL</li>
                                    <li>Discord URL</li>
                                </ul>
                            </li>
                            <li>Now Submit Your Details.</li>
                            <li>You can check your coin Details in My Account <i class="fas fa-arrow-right"></i> Coin List</li>
                            <li>After that MN ROI team will verify all Platform. If everything goes well than your coin will be listed soon in MN Browsing platform otherwise we will contact you on discord for further detail.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block rounded">
                    <div class="block-header bg-black-op-25">
                        <h3 class="block-title  text-center">Coin Details</h3>
                    </div>
                    <div class="block-content">
                        <form action="{!! route('coin.mail') !!}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="name">Coin Name</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Your Coin Name" value="{{ \Request::old('name') }}" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="ticker">Coin Ticker</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="ticker" name="ticker" placeholder="Please Enter Your Coin Ticker" value="{{ \Request::old('ticker') }}" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="explorer_url">Explorer Url</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="explorer_url" name="explorer_url" placeholder="Please Enter Your Explorer Url" value="{{ \Request::old('explorer_url') }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="github_url">Github Url</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="github_url" name="github_url" placeholder="Please Enter Your Github Url" value="{{ \Request::old('github_url') }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="website_url">Website Url</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="website_url" name="website_url" placeholder="Please Enter Your Website Url" value="{{ \Request::old('website_url') }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="bitcointalk_url">BitcoinTalk Url</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="bitcointalk_url" name="bitcointalk_url" placeholder="Please Enter Your BitcoinTalk Url" value="{{ \Request::old('bitcointalk_url') }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="discord_url">Discord Url</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="dicord_url" name="discord_url" placeholder="Please Enter Your Discord Url" value="{{ \Request::old('discord_url') }}" autocomplete="off">
                                </div>
                            </div>

                            <div class="text-center">
                                <div class="g-recaptcha" data-sitekey="6LfbbpMUAAAAALk5uDdCM1CWfrcB6yfQ6c3gYjX5"></div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-9 ">
                                    <a href="{!! route('welcome') !!}" class="offset-5"> <button class="btn btn-outline-purple"><i class="fas fa-chevron-circle-left"></i> Back</button></a>
                                    <button type="submit" class="btn btn-outline-success offset-1"><i class="fas fa-plus-circle"></i> Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


</div>

<!-- END Page Content -->
</main>
<!-- END Main Container -->

<!-- Footer -->
<footer id="page-footer" style="background: rgba(0,0,0,0.9); position: absolute; bottom: 0; width: 100%;">
    <div class="content py-20 font-size-xs clearfix">
        {{--<div class="float-right">
            Developed with <i class="fa fa-heart" style="color: red;"></i> by
            <a class="font-w600" href="https://www.groupghetto.com" target="_blank">GG Dev</a>
        </div>--}}
        <div class="float-left">
            <span class="font-w600">Copyright</span> &copy; <span class="js-year-copy">2019</span> MN Browsing
        </div>
    </div>
</footer>
<!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{!! asset('assets/js/core/jquery.min.js') !!}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="{!! asset('assets/js/core/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.appear.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.countTo.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/js.cookie.min.js') !!}"></script>
<script src="{!! asset('assets/js/codebase.js') !!}"></script>
<!-- Page JS Plugins -->
<script src="{!! asset('assets/js/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') !!}"></script>
<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/be_tables_datatables.js') !!}"></script>

<script src="{!! asset('assets/js/plugins/chartjs/Chart.bundle.min.js') !!}"></script>

<!-- Page JS Code -->
{{--<script src="{!! asset('assets/js/pages/be_pages_crypto_dashboard.js') !!}"></script>--}}

</body>
</html>
