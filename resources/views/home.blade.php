@extends('layouts.app')
@push('styles')
    <style>
        a{ color: #7e7ee8 !important }
    </style>
@endpush
@section('content')
    @include('layouts.alerts')
    <div class="row">
        <div class="col-12">
            <a href="{!! route('coins.create') !!}" class="float-right">
                <button class="btn btn-outline-purple"><i class="fa fa-plus-circle"></i> Add New Coin</button>
            </a>
        </div>
    </div>
    <br>
    <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer" id="DataTables_Table_1" role="grid">
                    <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1"
                            colspan="1" aria-label="Name: activate to sort column ascending">Name
                        </th>
                        <th class="d-none d-sm-table-cell sorting" tabindex="0"
                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1">
                            VOLUME
                        </th>
                        <th class="d-none d-sm-table-cell sorting_desc" style="width: 15%;" tabindex="0"
                            aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                            aria-label="Access: activate to sort column ascending" aria-sort="descending">
                            MARKET CAP
                        </th>
                        <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                            aria-label="Profile">PRICE
                        </th>
                        <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                            aria-label="Profile">CHANGE
                        </th>
                        <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                            aria-label="Profile">ROI
                        </th>
                        <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                            aria-label="Profile">MN WORTH
                        </th>
                        <th class="text-center sorting_disabled" style="width: 15%;" rowspan="1" colspan="1"
                            aria-label="Profile">STATUS
                        </th>
                        <td>
                            Action
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                @if(isset($coins) && count($coins))
                    @foreach($coins as $coin)
                        @php
                            $status = 'NORMAL';
                            if ($coin->ico == '1') {
                                $status = 'ICO';
                            }elseif ($coin->coming_soon == '1') {
                                $status = 'COMING SOON';
                            }elseif ($coin->maintenance == '1') {
                                $status = 'MAINTENANCE';
                            }elseif ($coin->simple == '1') {
                                $status = 'SIMPLE STATICS';
                            }
                        @endphp
                        <tr role="row" class="odd">
                            <td class="text-center"><a href="{!! route('coin.name',$coin->name) !!}" target="_blank"><img src="{!! $coin->icon_url !!}" alt="{!! $coin->name !!}" width="20"> <strong>{!! $coin->name !!}</strong>  </a></td>
                            <td class="font-w600">{!! number_format((float)$usd_24h_vol[$coin->name], 2, '.', ',') !!} $</td>
                            <td class="d-none d-sm-table-cell">{!! number_format((float)$market_cap[$coin->name], 2, '.', ',') !!} $</td>
                            <td class="d-none d-sm-table-cell">{!! $usd[$coin->name] !!} $</td>
                            <td class="d-none d-sm-table-cell"> <span style="color: {!! $usd_24h_change[$coin->name] < 0 ? 'red' : 'green' !!}">% {!! number_format((float)$usd_24h_change[$coin->name], 2, '.', ',') !!} <i class="fa fa-angle-double-{!! $usd_24h_change[$coin->name] < 0 ? 'down' : 'up' !!}"></i> </span></td>
                            <td class="d-none d-sm-table-cell">{!! number_format((float)$roi_percent[$coin->name], 2, '.', ',').'% / '.(int)$roi_days[$coin->name] !!} Days</td>
                            <td class="d-none d-sm-table-cell">{!! number_format((float)$mn_worth[$coin->name], 3, '.', ',') !!} $</td>
                            <td class="d-none d-sm-table-cell">{!! $status !!}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{!! route('coins.edit',$coin->id) !!}" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="Edit" data-original-title="Edit">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                    <form action="{!! route('coins.destroy',$coin->id) !!}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="Delete" data-original-title="Delete">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                    <a href="{!! route('coin.social',$coin->id) !!}" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="Social Media" data-original-title="Social">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
          </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
