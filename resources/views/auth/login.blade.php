<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>MN BROWSING Login</title>

    <meta name="description" content="MN BROWSING LOGIN">
    <meta name="author" content="Ryan Dev">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="MN BROWSING">
    <meta property="og:site_name" content="MN BROWSING">
    <meta property="og:description" content="MN BROWSING">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{!! asset('images/icon.ico') !!}">
    <link rel="icon" type="image/png" sizes="192x192" href="{!! asset('images/icon.ico' ) !!}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!! asset('images/icon.ico') !!}">
    <!-- END Icons -->

    <!-- Stylesheets -->

    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{!! asset('assets/css/codebase.min.css') !!}">

</head>
<body>

<div id="page-container" class="main-content-boxed side-trans-enabled">

    <!-- Main Container -->
    <main id="main-container" style="min-height: 669px;">

        <!-- Page Content -->
        <div class="bg-body-dark bg-pattern" style="background-image: url('{!! asset('assets/media/various/bg-pattern-inverse.png') !!}');">
            <div class="row mx-0 justify-content-center">
                <div class="hero-static col-lg-6 col-xl-4">
                    <div class="content content-full overflow-hidden">
                        <!-- Header -->
                        <div class="py-30 text-center">
                            <a class="link-effect font-w700" href="{!! route('welcome') !!}">
                                <img src="{!! asset('images/logo-i.png') !!}" alt="MN BROWSING" width="150">
                            </a>
                            <h1 class="h4 font-w700 mt-30 mb-10">Welcome to Your Dashboard</h1>
                            <h2 class="h5 font-w400 text-muted mb-0">It’s a great day today!</h2>
                        </div>
                        <!-- END Header -->

                        <form class="js-validation-signin" method="POST" action="{{ route('login') }}" novalidate="novalidate">
                            @csrf
                            <div class="block block-themed block-rounded block-shadow">
                                <div class="block-header bg-muted">
                                    <h3 class="block-title">Please Login</h3>
                                </div>
                                <div class="block-content">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="login-username">E-mail</label>
                                            {{--<input type="text" class="form-control" id="login-username" name="email">--}}
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="login-password">Password</label>
                                            {{--<input type="password" class="form-control" id="login-password" name="password">--}}
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-sm-12 text-sm-right push">
                                            <button type="submit" class="btn btn-alt-primary float-right">
                                                <i class="si si-login mr-10"></i> Login
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="block-content bg-body-light">
                                    <div class="form-group text-center">
                                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_signup3.html">
                                            <i class="fa fa-plus mr-5"></i> Create Account
                                        </a>
                                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_reminder3.html">
                                            <i class="fa fa-warning mr-5"></i> Forgot Password
                                        </a>
                                    </div>
                                </div>--}}
                            </div>
                        </form>
                        <!-- END Sign In Form -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->

    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{!! asset('assets/js/core/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/bootstrap.bundle.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery-scrollLock.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.appear.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/jquery.countTo.min.js') !!}"></script>
<script src="{!! asset('assets/js/core/js.cookie.min.js') !!}"></script>
<script src="{!! asset('assets/js/codebase.js') !!}"></script>

<!-- Page JS Plugins -->
<script src="{!! asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') !!}"></script>

<!-- Page JS Code -->
<script src="{!! asset('assets/js/pages/op_auth_signin.js') !!}"></script>

</body></html>
