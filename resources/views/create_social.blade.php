@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-2">
    @include('layouts.alerts')
    <div class="block">
        <div class="block-header bg-black-op-25">
            <h3 class="block-title text-light">Social Media</h3>
        </div>

        <div class="block-content">
            <form action="{!! isset($coin->social) ? route('social.update',$coin->social->id) : route('social.store') !!}" method="post">
                @csrf

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="website">Website URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="website" name="website" placeholder="Enter Website URL" value="{!! isset($coin->social->website) ? $coin->social->website : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="github">Github URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="github" name="github" placeholder="Enter Github URL" value="{!! isset($coin->social->github) ? $coin->social->github : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="marketcap">Marketcap URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="marketcap" name="marketcap" placeholder="Enter Marketcap URL" value="{!! isset($coin->social->marketcap) ? $coin->social->marketcap : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="explorer">Explorer URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="explorer" name="explorer" placeholder="Enter Explorer URL" value="{!! isset($coin->social->explorer) ? $coin->social->explorer : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="twitter">Twitter URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Enter Twitter URL" value="{!! isset($coin->social->twitter) ? $coin->social->twitter : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="facebook">Facebook URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Enter Facebook URL" value="{!! isset($coin->social->facebook) ? $coin->social->facebook : null !!}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="discord">Discord URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="discord" name="discord" placeholder="Enter Discord URL" value="{!! isset($coin->social->discord) ? $coin->social->discord : null !!}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="reddit">Reddit URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="reddit" name="reddit" placeholder="Enter Reddit URL" value="{!! isset($coin->social->reddit) ? $coin->social->reddit : null !!}">
                    </div>
                </div>

                <input type="hidden" name="coin_name" value="{!! $coin->name !!}">
                <input type="hidden" name="coin_id" value="{!! $coin->id !!}">
                <div class="form-group row">
                    <div class="col-lg-9 ml-auto">
                        <a href="{!! route('coins.index') !!}" class="offset-5"> <button class="btn btn-outline-purple"><i class="fas fa-chevron-circle-left"></i> Back</button></a>
                        <button type="submit" class="btn btn-outline-success offset-1"><i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
