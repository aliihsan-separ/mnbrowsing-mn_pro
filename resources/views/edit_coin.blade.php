@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-2">
    @include('layouts.alerts')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Edit {{ $coin->name }}</h3>
        </div>
        <div class="block-content">
            <form action="{!! route('coins.update',$coin->id) !!}" method="post">
                @csrf @method('PUT')
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="name">Coin Name</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Coin Name" required autocomplete="off" value="{{ $coin->name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="ticker">Coin Ticker</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="ticker" name="ticker" placeholder="Enter Coin's Ticker" required autocomplete="off" value="{{ $coin->ticker }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="exchange_api_url">Exchange API Url</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="exchange_api_url" name="exchange_api_url" placeholder="Enter Coin's Exchange API Url" required autocomplete="off" value="{{ $coin->exchange_api_url }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_count_api_url">Masternode Count API Url</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_count_api_url" name="mn_count_api_url" placeholder="Enter Coin's Masternode Count Url" required autocomplete="off" value="{{ $coin->mn_count_api_url }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="circulation_supply_api_url">Circulation API Url</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="circulation_supply_api_url" name="circulation_supply_api_url" placeholder="Enter Coin's Circulation API Url" required autocomplete="off" value="{{ $coin->circulation_supply_api_url }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="paid_rewards_mn">Paid Rewards For Masternodes</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="paid_rewards_mn" name="paid_rewards_mn" placeholder="Enter Paid Rewards For Masternodes" required autocomplete="off" value="{{ $coin->paid_rewards_mn }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_required_coins">MasterNode Required Coins</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_required_coins" name="mn_required_coins" placeholder="MasterNode Required Coins" required autocomplete="off" value="{{ $coin->mn_required_coins }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="mn_block_reward">MasterNode Block Reward</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="mn_block_reward" name="mn_block_reward" placeholder="MasterNode Required Coins (Enter In Coin Type)" required autocomplete="off" value="{{ $coin->mn_block_reward }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="icon_url">Icon URL</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control" id="icon_url" name="icon_url" placeholder="Enter Coin's Icon URL" required autocomplete="off" value="{{ $coin->icon_url }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="icon_url">Coin Display Type</label>
                    <div class="col-lg-7">
                        <label class="css-control css-control-success css-radio hide">
                            <input type="radio" class="css-control-input" name="display_type" value="0" {{ ($coin->ico == '0' && $coin->maintenance == '0' && $coin->coming_soon == '0' && $coin->simple == '0') ? 'checked' : '' }}>
                            <span class="css-control-indicator"></span> NORMAL
                        </label>
                        <label class="css-control css-control-success css-radio  hide">
                            <input type="radio" class="css-control-input" name="display_type" value="1" {{ $coin->simple == '1' ? 'checked' : '' }}>
                            <span class="css-control-indicator"></span> SIMPLE STATIC
                        </label>
                        <label class="css-control css-control-success css-radio show">
                            <input type="radio" class="css-control-input" name="display_type" value="1" {{ $coin->ico == '1' ? 'checked' : '' }}>
                            <span class="css-control-indicator"></span> ICO
                        </label>
                        <label class="css-control css-control-success css-radio show">
                            <input type="radio" class="css-control-input" name="display_type" value="2" {{ $coin->coming_soon == '1' ? 'checked' : '' }} >
                            <span class="css-control-indicator"></span> COMING SOON
                        </label>
                        <label class="css-control css-control-success css-radio show">
                            <input type="radio" class="css-control-input" name="display_type" value="3" {{ $coin->maintenance == '1' ? 'checked' : '' }}>
                            <span class="css-control-indicator"></span> MAINTENANCE
                        </label>
                    </div>
                </div>

                <div class="form-group row notes" style="display: none;">
                    <label class="col-lg-4 col-form-label" for="notes">Notes</label>
                    <div class="col-lg-7">
                        <textarea class="form-control" id="notes" name="notes" rows="6" placeholder="Notes.." style="margin-top: 0px; margin-bottom: 0px; height: 137px;">{{ isset($coin->notes) ? $coin->notes : '' }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-9 ml-auto">
                        <a href="{!! route('coins.index') !!}" class="btn btn-alt-danger offset-5"> Back</a>
                        <button type="submit" class="btn btn-alt-primary offset-1"> Edit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.show').on('click',function () {
            $('.notes').removeAttr("style");
        });
        $('.hide').on('click',function () {
            $('.notes').css({'display':'none'});
        })
        if ($('.show input')) {
            $('.notes').removeAttr("style");
        }
    </script>
@endpush
