<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Hash Admin',
            'email' => 'groupghetto@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now(),
            'password' => '$2y$12$H4qLsFa5c3rj0WAe92yvNOWl8UlOOphpHtvvslTtIbr6qrIrQiZWu',
            'is_admin' => '1',
        ]);
    }
}
