<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ticker');
            $table->string('exchange_api_url')->nullable();
            $table->string('paid_rewards_mn')->nullable();
            $table->string('mn_required_coins')->nullable();
            $table->string('mn_block_reward')->nullable();
            $table->string('circulation_supply_api_url')->nullable();
            $table->string('mn_count_api_url')->nullable();
            $table->string('volume',12,10)->default(0);
            $table->string('volume_usd',8,4)->default(0);
            $table->string('market_cap',8,4)->default(0);
            $table->string('price',8,4)->default(0);
            $table->integer('mn_count')->default(0);
            $table->string('change',8,4)->default(0);
            $table->string('circulation',8,4)->default(0);
            $table->string('daily_reward',8,4)->default(0);
            $table->integer('roi_days')->default(0);
            $table->string('roi_percent',8,4)->default(0);
            $table->string('mn_worth',8,4)->default(0);
            $table->enum('coming_soon',['0','1']);
            $table->enum('ico',['0','1']);
            $table->enum('maintenance',['0','1']);;
            $table->string('notes')->nullable();
            $table->string('icon_url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
