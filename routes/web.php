<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Psr7\Request;

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();

Route::get('/coin/{coin_name}','CoinController@show')->name('coin.name');

Route::get('/coin-listing',function (){
    return view('coin-listing');
})->name('coin-listing');


Route::post('/coin-mail','CoinController@coinMail')->name('coin.mail');

Route::middleware(['auth'])->group(function () {

    Route::get('/admin/login', 'Auth\LoginController@login')->name('admin.login');

    Route::get('/home', 'CoinController@index')->name('home');

    Route::resource('coins','CoinController');

    Route::get('/coin/social/{id}','CoinController@social')->name('coin.social');
    Route::post('/coin/social/store','CoinController@social_store')->name('social.store');
    Route::post('/coin/social/update/{id}','CoinController@social_update')->name('social.update');

});


Route::get('/deneme',function (){
/*    $api = new CoingeckoApi();
dd($api->shared()->getExchangeRates());
    $timestamp = $api->shared()->priceCharts(Api::BASE_ETH, Api::QUOTE_USD, Api::PERIOD_24HOURS, true);
    dd($timestamp);*/

//"id": "dash",
//    "symbol": "dash",
//    "name": "Dash"

    $client = new \GuzzleHttp\Client();
    /**
       return coin id,symbol and name
    */
    $response1 = $client->get('https://api.coingecko.com/api/v3/coins/list');

    /**
     * return coin btc price
     */
    $response2 = $client->get('https://api.coingecko.com/api/v3/simple/price?ids=dash&vs_currencies=btc');
    echo "<pre>";
    dd(json_decode((string)$response1->getBody()));

});
